package com.manchezv.onroute.utils

data class DateDifference(val hours: Long, val minutes: Long, val seconds: Long);