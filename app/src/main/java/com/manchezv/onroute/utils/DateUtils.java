package com.manchezv.onroute.utils;

import java.util.Date;

public class DateUtils {

    private static final long secondsInMilli = 1000;
    private static final long minutesInMilli = secondsInMilli * 60;
    private static final long hoursInMilli = minutesInMilli * 60;

    public static DateDifference getDifferenceFor(Date startDate, Date endDate) {
        long difference = endDate.getTime() - startDate.getTime();

        long hours = difference / hoursInMilli;
        difference = difference % hoursInMilli;

        long minutes = difference / minutesInMilli;
        difference = difference % minutesInMilli;

        long seconds = difference / secondsInMilli;

        return new DateDifference(hours, minutes, seconds);
    }
}
