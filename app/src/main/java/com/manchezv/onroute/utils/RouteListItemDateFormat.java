package com.manchezv.onroute.utils;

import java.text.SimpleDateFormat;

import javax.inject.Inject;

public class RouteListItemDateFormat extends SimpleDateFormat {

    private static final String itemDateFormat = "dd/MM/yyyy HH:mm:ss";

    @Inject
    public RouteListItemDateFormat() {
        super(itemDateFormat);
    }
}
