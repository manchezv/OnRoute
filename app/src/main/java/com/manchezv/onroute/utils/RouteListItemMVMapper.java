package com.manchezv.onroute.utils;

import android.support.annotation.NonNull;

import com.manchezv.onroute.model.repositories.RouteListItem;
import com.manchezv.onroute.viewmodels.model.RouteListItemModelView;

import javax.inject.Inject;

public class RouteListItemMVMapper extends EntityMapper<RouteListItem, RouteListItemModelView> {

    private final RouteListItemDateFormat dateFormat;

    @Inject
    public RouteListItemMVMapper(RouteListItemDateFormat dateFormat) {
        this.dateFormat = dateFormat;
    }

    @Override
    protected RouteListItem mapOperation(@NonNull RouteListItemModelView element) {
        throw new RuntimeException("SummaryRowMapper map operation not implemented");
    }

    @Override
    protected RouteListItemModelView reverseMapOperation(@NonNull RouteListItem element) {
        DateDifference difference = DateUtils.getDifferenceFor(element.getStartDate(), element.getEndDate());
        return new RouteListItemModelView(
                element.getRouteId(),
                element.getRouteName(),
                dateFormat.format(element.getStartDate()),
                (int) difference.getHours(),
                (int) difference.getMinutes(),
                (int) difference.getSeconds(),
                element.getPositions()
        );
    }
}
