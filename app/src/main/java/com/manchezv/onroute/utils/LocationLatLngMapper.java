package com.manchezv.onroute.utils;

import android.support.annotation.NonNull;

import com.google.android.gms.maps.model.LatLng;
import com.manchezv.onroute.model.db.entities.LocationEntity;
import com.manchezv.onroute.model.repositories.RouteListItem;
import com.manchezv.onroute.viewmodels.model.RouteListItemModelView;

import javax.inject.Inject;

public class LocationLatLngMapper extends EntityMapper<LocationEntity, LatLng> {

    private final RouteListItemDateFormat dateFormat;

    @Inject
    public LocationLatLngMapper(RouteListItemDateFormat dateFormat) {
        this.dateFormat = dateFormat;
    }

    @Override
    protected LocationEntity mapOperation(@NonNull LatLng element) {
        throw new RuntimeException("SummaryRowMapper map operation not implemented");
    }

    @Override
    protected LatLng reverseMapOperation(@NonNull LocationEntity element) {
        return new LatLng(element.getLatitude(), element.getLongitude());
    }
}
