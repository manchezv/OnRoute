package com.manchezv.onroute.services;

import android.content.Intent;
import android.location.Location;
import android.os.Binder;
import android.os.IBinder;
import android.os.Looper;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.manchezv.onroute.model.session.RouteSession;
import com.manchezv.onroute.views.components.notifications.NotificationsBuilder;

import javax.inject.Inject;

import dagger.android.DaggerService;

public class LocatorService extends DaggerService {

    public static final String EXTRA_LOCATION = "com.manchezv.onroute.services.LocatorService:EXTRA_LOCATION";

    public static final String ACTION_LOCATION_UPDATE = "com.manchezv.onroute.services.LocatorService.LocationBroadcast";

    private static final int ONGOING_NOTIFICATION_ID = 2999;

    private final IBinder mBinder = new LocalBinder();

    private LocationRequest mLocationRequest;

    private FusedLocationProviderClient mFusedLocationClient;

    private LocationCallback mLocationCallback;

    @Inject
    NotificationsBuilder notificationBuilder;

    @Inject
    RouteSession routeSession;


    public LocatorService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                onNewLocation(locationResult.getLastLocation());
            }
        };

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);


    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public void startRoute() {
        routeSession.startRoute();
        startService(new Intent(getApplicationContext(), LocatorService.class));
        startForeground(ONGOING_NOTIFICATION_ID, notificationBuilder.buildOngoingNotification());

        try {
            mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                    mLocationCallback, Looper.myLooper());
        } catch (SecurityException unlikely) {
            routeSession.finishRoute();
        }
    }

    public void stopRoute() {
        try {
            stopForeground(true);
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
            routeSession.stopRoute();
            stopSelf();
        } catch (SecurityException ex) {
            routeSession.finishRoute();
        }
    }

    private void onNewLocation(Location location) {
        routeSession.addLocation(location);
    }

    public class LocalBinder extends Binder {
        public LocatorService getService() {
            return LocatorService.this;
        }
    }
}
