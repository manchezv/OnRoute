package com.manchezv.onroute.views.main;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.manchezv.onroute.R;
import com.manchezv.onroute.viewmodels.MainViewModel;
import com.manchezv.onroute.views.BaseLocationActivity;
import com.manchezv.onroute.views.components.navigator.Navigator;
import com.manchezv.onroute.views.components.sectionnavigator.SectionNavigator;
import com.manchezv.onroute.views.main.routecontrol.RouteNameDialog;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasFragmentInjector;
import dagger.android.support.HasSupportFragmentInjector;

public class MainActivity extends BaseLocationActivity implements HasSupportFragmentInjector {

    @Inject
    SectionNavigator sectionNavigator;

    @Inject
    DispatchingAndroidInjector<Fragment> fragmentInjector;


    @Inject
    Navigator navigator;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.navigation)
    BottomNavigationView navigation;

    @BindView(R.id.routeFABButton)
    FloatingActionButton routeFabButton;

    private MainViewModel viewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setSupportActionBar(toolbar);

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        if (savedInstanceState == null) {
            navigation.setSelectedItemId(R.id.nav_control);
        }

        viewModel = ViewModelProviders.of(this, getViewModelFactory()).get(MainViewModel.class);

        if (viewModel.isServiceInRoute().getValue() != null && viewModel.isServiceInRoute().getValue()) {
            if (!hasLocationPermission()) {
                requestPermissions();
            }
        }

        viewModel.isServiceInRoute().observe(this, aBoolean -> routeFabButton.setActivated(aBoolean != null && aBoolean));
    }

    @Override
    protected void initDagger() {
        AndroidInjection.inject(this);
    }

    @Override
    public int getLayoutResId() {
        return R.layout.activity_main;
    }

    @OnClick(R.id.routeFABButton)
    void onFabClicked(View v) {
        if (!hasLocationPermission()) {
            requestPermissions();
        } else if (isBound()) {
            if (v.isActivated()) {
                stopRoute();
                navigator.displayDialogName(new RouteNameDialog.OnActionDone() {
                    @Override
                    public void onNameInserted(String name) {
                        viewModel.onNameInserted(name);
                    }

                    @Override
                    public void onCancelClicked() {
                        viewModel.finishRoute();
                    }
                });
            } else {
                startRoute();
            }
        }
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.nav_control:
                    sectionNavigator.navigateToControl();
                    return true;
                case R.id.nav_list:
                    sectionNavigator.navigateToRouteList();
                    return true;
                default:
                    return false;
            }
        }
    };

    public void showFab(boolean show) {
        if (show) {
            routeFabButton.show();
        } else {
            routeFabButton.hide();
        }
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentInjector;
    }
}
