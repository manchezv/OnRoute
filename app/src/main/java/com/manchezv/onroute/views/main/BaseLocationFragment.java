package com.manchezv.onroute.views.main;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;

import com.manchezv.onroute.services.LocatorService;
import com.manchezv.onroute.views.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public abstract class BaseLocationFragment extends BaseFragment {

    protected abstract void setLocation(String altitude, String latitude, String longitude);

    protected abstract void setNoLocation();


}
