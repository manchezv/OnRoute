package com.manchezv.onroute.views.main.routelist;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.manchezv.onroute.R;
import com.manchezv.onroute.viewmodels.model.RouteListItemModelView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RoutesAdapter extends ListAdapter<RouteListItemModelView, RoutesAdapter.RouteViewHolder> {

    private final Context context;
    private OnClickListener listener;
    private LayoutInflater layoutInflater;

    public RoutesAdapter(Context context, OnClickListener listener) {
        super(DIFF_CALLBACK);
        this.context = context;
        this.listener = listener;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public RouteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_route, parent, false);
        return new RouteViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull RouteViewHolder holder, int position) {
        holder.bindItem(getItem(position), listener);
    }

    public static final DiffUtil.ItemCallback<RouteListItemModelView> DIFF_CALLBACK =
            new DiffUtil.ItemCallback<RouteListItemModelView>() {
                @Override
                public boolean areItemsTheSame(RouteListItemModelView oldItem, RouteListItemModelView newItem) {
                    return oldItem.getRouteId() == newItem.getRouteId();
                }

                @Override
                public boolean areContentsTheSame(RouteListItemModelView oldItem, RouteListItemModelView newItem) {
                    return oldItem.getRouteId() == newItem.getRouteId()
                            && oldItem.getRouteName().equalsIgnoreCase(newItem.getRouteName())
                            && oldItem.getPositions() == newItem.getPositions();
                }
            };


    class RouteViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvRouteName)
        TextView tvRouteName;

        @BindView(R.id.tvDuration)
        TextView tvDuration;

        @BindView(R.id.tvDate)
        TextView tvDate;

        @BindView(R.id.tvPositions)
        TextView tvPositions;

        RouteViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bindItem(RouteListItemModelView route, OnClickListener listener) {
            tvRouteName.setText(route.getRouteName());
            tvDuration.setText(
                    context.getString(R.string.duration,
                            getQuantityString(R.plurals.hours, route.getHourDuration()),
                            getQuantityString(R.plurals.minutes, route.getMinuteDuration()),
                            getQuantityString(R.plurals.seconds, route.getSecondsDuration())
                    )
            );
            tvDate.setText(route.getStartDate());
            tvPositions.setText(getQuantityString(R.plurals.positions, (int) route.getPositions()));

            itemView.setOnClickListener(v -> {
                if (listener != null) {
                    listener.onRouteSelected(route.getRouteId());
                }
            });
        }

        @NonNull
        private String getQuantityString(int plural, int duration) {
            return context.getResources().getQuantityString(plural, duration, duration);
        }
    }
}
