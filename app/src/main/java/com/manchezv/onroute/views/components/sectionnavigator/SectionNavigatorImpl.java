package com.manchezv.onroute.views.components.sectionnavigator;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.manchezv.onroute.R;
import com.manchezv.onroute.views.main.routecontrol.RouteControlFragment;
import com.manchezv.onroute.views.main.routelist.RouteListFragment;

import javax.inject.Inject;

public class SectionNavigatorImpl implements SectionNavigator {

    private final AppCompatActivity activity;

    @Inject
    public SectionNavigatorImpl(AppCompatActivity activity) {
        this.activity = activity;
    }

    @Override
    public void navigateToControl() {
        if(!isCurrentFragment(RouteControlFragment.TAG)) {
            replaceFragment(RouteControlFragment.newInstance(), RouteControlFragment.TAG);
        }
    }

    @Override
    public void navigateToRouteList() {
        if(!isCurrentFragment(RouteListFragment.TAG)) {
            replaceFragment(RouteListFragment.newInstance(), RouteListFragment.TAG);
        }
    }

    private boolean isCurrentFragment(String progressTag) {
        Fragment fragment = activity.getSupportFragmentManager().findFragmentByTag(progressTag);
        return (fragment != null && fragment.isVisible());
    }

    private void replaceFragment(Fragment fragment, String tag) {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
                .replace(
                        R.id.lyContent,
                        fragment,
                        tag
                )
                .commit();
    }
}
