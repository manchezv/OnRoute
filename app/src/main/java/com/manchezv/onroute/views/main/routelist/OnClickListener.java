package com.manchezv.onroute.views.main.routelist;

public interface OnClickListener {
    void onRouteSelected(long routeId);
}
