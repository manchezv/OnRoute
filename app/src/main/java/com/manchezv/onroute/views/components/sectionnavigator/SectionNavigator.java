package com.manchezv.onroute.views.components.sectionnavigator;

public interface SectionNavigator {

    void navigateToControl();

    void navigateToRouteList();
}
