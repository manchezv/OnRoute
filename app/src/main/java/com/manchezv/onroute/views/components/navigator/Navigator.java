package com.manchezv.onroute.views.components.navigator;

import com.manchezv.onroute.views.main.routecontrol.RouteNameDialog;

public interface Navigator {
    void goToMap(long routeId);
    void displayDialogName(RouteNameDialog.OnActionDone listener);
}
