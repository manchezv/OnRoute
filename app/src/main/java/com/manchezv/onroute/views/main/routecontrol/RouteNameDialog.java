package com.manchezv.onroute.views.main.routecontrol;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.manchezv.onroute.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class RouteNameDialog extends AppCompatDialogFragment {

    public static final String TAG = RouteNameDialog.class.getSimpleName();

    private OnActionDone listener;

    @BindView(R.id.tlyName)
    TextInputLayout tLyName;

    @BindView(R.id.etName)
    TextInputEditText etName;

    private Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = getActivity().getLayoutInflater().inflate(R.layout.dialog_name, container);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) {
            unbinder.unbind();
        }
    }


    public static DialogFragment newInstance(OnActionDone listener) {
        RouteNameDialog dialog = new RouteNameDialog();
        dialog.listener = listener;
        return dialog;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setCancelable(false);
    }

    @OnClick(R.id.btSave)
    void onSaveClicked() {
        if(etName.getText() == null || etName.getText().toString().isEmpty()) {
            tLyName.setError(getString(R.string.invalid_name));
        } else if(listener != null){
            listener.onNameInserted(etName.getText().toString());
            dismiss();
        } else {
            dismiss();
        }
    }

    @OnClick(R.id.btCancel)
    void onCancelClicked() {
        if(listener != null) {
            listener.onCancelClicked();
        }

        dismiss();
    }


    public interface OnActionDone {

        void onNameInserted(String name);

        void onCancelClicked();
    }
}
