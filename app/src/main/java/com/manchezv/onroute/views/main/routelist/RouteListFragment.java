package com.manchezv.onroute.views.main.routelist;


import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.manchezv.onroute.R;
import com.manchezv.onroute.viewmodels.MainViewModel;
import com.manchezv.onroute.views.BaseFragment;
import com.manchezv.onroute.views.components.navigator.Navigator;
import com.manchezv.onroute.views.main.MainActivity;

import javax.inject.Inject;

import butterknife.BindView;
import dagger.android.AndroidInjection;
import dagger.android.support.AndroidSupportInjection;
import dagger.android.support.AndroidSupportInjectionModule;

/**
 * A simple {@link Fragment} subclass.
 */
public class RouteListFragment extends BaseFragment {

    public static final String TAG = RouteListFragment.class.getSimpleName();
    private RoutesAdapter adapter;

    @BindView(R.id.rvRoutes)
    RecyclerView rvRoutes;

    @Inject
    Navigator navigator;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    public RouteListFragment() {
        // Required empty public constructor
    }

    public static RouteListFragment newInstance() {
        return new RouteListFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        AndroidSupportInjection.inject(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).showFab(false);
        }

        adapter = new RoutesAdapter(getActivity(), new OnClickListener() {
            @Override
            public void onRouteSelected(long routeId) {
                navigator.goToMap(routeId);
            }
        });

        rvRoutes.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvRoutes.setHasFixedSize(true);
        rvRoutes.setAdapter(adapter);
        rvRoutes.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));

        MainViewModel viewModel = ViewModelProviders.of(getActivity(), viewModelFactory).get(MainViewModel.class);

        viewModel.getRouteListInfo().observe(this, routeListItemModelViews -> adapter.submitList(routeListItemModelViews));

    }

    @Override
    public int getLayoutResId() {
        return R.layout.fragment_list_route;
    }
}
