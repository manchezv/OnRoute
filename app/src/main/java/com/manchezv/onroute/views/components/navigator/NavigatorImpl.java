package com.manchezv.onroute.views.components.navigator;

import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;

import com.manchezv.onroute.views.main.routecontrol.RouteNameDialog;
import com.manchezv.onroute.views.map.MapActivity;

import javax.inject.Inject;

public class NavigatorImpl implements Navigator {

    private final AppCompatActivity activity;

    @Inject
    public NavigatorImpl(AppCompatActivity activity) {
        this.activity = activity;
    }

    @Override
    public void goToMap(long routeId) {
        activity.startActivity(MapActivity.getLaunchIntent(activity, routeId));
    }

    @Override
    public void displayDialogName(RouteNameDialog.OnActionDone listener) {
        DialogFragment newFragment = RouteNameDialog.newInstance(listener);
        newFragment.show(activity.getSupportFragmentManager(), RouteNameDialog.TAG);
    }
}
