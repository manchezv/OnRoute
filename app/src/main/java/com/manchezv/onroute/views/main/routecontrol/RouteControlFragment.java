package com.manchezv.onroute.views.main.routecontrol;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.manchezv.onroute.R;
import com.manchezv.onroute.model.db.entities.LocationEntity;
import com.manchezv.onroute.services.LocatorService;
import com.manchezv.onroute.viewmodels.MainViewModel;
import com.manchezv.onroute.views.BaseFragment;
import com.manchezv.onroute.views.components.navigator.Navigator;
import com.manchezv.onroute.views.main.BaseLocationFragment;
import com.manchezv.onroute.views.main.MainActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import dagger.android.AndroidInjection;
import dagger.android.support.AndroidSupportInjection;

/**
 * A simple {@link Fragment} subclass.
 */
public class RouteControlFragment extends BaseLocationFragment {

    public static final String TAG = RouteControlFragment.class.getSimpleName();

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @BindView(R.id.tvAltitude)
    TextView tvAltitude;

    @BindView(R.id.tvLongitude)
    TextView tvLongitude;

    @BindView(R.id.tvLatitude)
    TextView tvLatitude;


    public RouteControlFragment() {
        // Required empty public constructor
    }

    public static RouteControlFragment newInstance() {
        return new RouteControlFragment();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setNoLocation();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).showFab(true);
        }

        MainViewModel viewModel = ViewModelProviders.of(getActivity(), viewModelFactory).get(MainViewModel.class);

        viewModel.getLastLocation().observe(this, locationEntity -> {
            if (locationEntity == null) {
                setNoLocation();
            } else {
                setLocation(
                        String.valueOf(locationEntity.getAltitude()),
                        String.valueOf(locationEntity.getLatitude()),
                        String.valueOf(locationEntity.getLongitude())
                );
            }
        });
    }

    @Override
    protected void setLocation(String altitude, String latitude, String longitude) {
        tvAltitude.setText(altitude);
        tvLatitude.setText(latitude);
        tvLongitude.setText(longitude);
    }

    @Override
    protected void setNoLocation() {
        setLocation(getString(R.string.null_value), getString(R.string.null_value), getString(R.string.null_value));
    }

    @Override
    public int getLayoutResId() {
        return R.layout.fragment_control;
    }
}
