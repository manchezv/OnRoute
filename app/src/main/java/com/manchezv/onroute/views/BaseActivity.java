package com.manchezv.onroute.views;

import android.arch.lifecycle.ViewModelProvider;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.manchezv.onroute.R;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BaseActivity extends AppCompatActivity {

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    private Unbinder unbinder;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(getLayoutResId());

        unbinder = ButterKnife.bind(this);

        initDagger();
    }



    public ViewModelProvider.Factory getViewModelFactory() {
        return viewModelFactory;
    }

    protected abstract void initDagger();

    public void showSnackbar(String text, int buttonTextRes, View.OnClickListener listener) {
        Snackbar.make(
                findViewById(android.R.id.content),
                text,
                Snackbar.LENGTH_INDEFINITE)
                .setAction(buttonTextRes, listener)
                .show();
    }

    public abstract int getLayoutResId();
}
