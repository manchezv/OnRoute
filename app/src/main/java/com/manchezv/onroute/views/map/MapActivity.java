package com.manchezv.onroute.views.map;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.JointType;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.RoundCap;
import com.manchezv.onroute.R;
import com.manchezv.onroute.model.db.entities.LocationEntity;
import com.manchezv.onroute.viewmodels.MainViewModel;
import com.manchezv.onroute.viewmodels.MapViewModel;
import com.manchezv.onroute.views.BaseActivity;
import com.manchezv.onroute.views.main.MainActivity;

import java.util.List;

import dagger.android.AndroidInjection;

public class MapActivity extends BaseActivity implements OnMapReadyCallback {

    private static final String ROUTE_ID = "com.manchezv.onroute.views.map:ROUTE_ID";

    private GoogleMap mMap;

    private MapViewModel viewModel;

    public static Intent getLaunchIntent(Activity activity, long routeId) {
        Intent intent = new Intent(activity, MapActivity.class);
        intent.putExtra(ROUTE_ID, routeId);
        return intent;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        viewModel = ViewModelProviders.of(this, getViewModelFactory()).get(MapViewModel.class);

        viewModel.setRouteId(getIntent().getLongExtra(ROUTE_ID, -1));

    }

    @Override
    protected void initDagger() {
        AndroidInjection.inject(this);
    }

    @Override
    public int getLayoutResId() {
        return R.layout.activity_map;
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

        viewModel.getRoutePositions().observe(this, latLngs -> {
            if (latLngs != null) {
                googleMap.clear();

                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                for(LatLng latlng : latLngs) {
                    builder.include(latlng);
                }

                Polyline route = googleMap.addPolyline(new PolylineOptions()
                        .clickable(true)
                        .addAll(latLngs));


                route.setStartCap(new RoundCap());
                route.setWidth(MapActivity.this.getResources().getDimension(R.dimen.line_map_width));
                route.setColor(ContextCompat.getColor(MapActivity.this, R.color.colorPrimaryDark));
                route.setJointType(JointType.ROUND);

                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(builder.build(), 50);
                googleMap.moveCamera(cameraUpdate);
            }
        });
    }
}
