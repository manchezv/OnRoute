package com.manchezv.onroute.views.components.notifications;

import android.app.Notification;

public interface NotificationsBuilder {
    Notification buildOngoingNotification();
}
