package com.manchezv.onroute.threading;

public interface MainThread {
    void runOnUiThread(Runnable runnable);
}
