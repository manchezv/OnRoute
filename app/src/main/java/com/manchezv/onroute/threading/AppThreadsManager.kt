package com.manchezv.onroute.threading

import android.os.Handler
import android.os.Looper
import java.util.concurrent.Executor
import java.util.concurrent.Executors
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
open class AppThreadsManager(
        private val diskIO: Executor,
        private val mainThread: MainThread
) {

    @Inject
    constructor(mainThread: MainThread) : this(
            Executors.newSingleThreadExecutor(),
            mainThread
    )

    fun getDiskIO() : Executor {
        return diskIO;
    }

    fun getMainThread() : MainThread {
        return mainThread;
    }

}