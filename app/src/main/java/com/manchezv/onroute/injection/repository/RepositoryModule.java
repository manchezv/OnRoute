package com.manchezv.onroute.injection.repository;

import com.manchezv.onroute.model.LocationsRepository;
import com.manchezv.onroute.model.RoutesRepository;
import com.manchezv.onroute.model.repositories.LocationsRepositoryImpl;
import com.manchezv.onroute.model.repositories.RoutesRepositoryImpl;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class RepositoryModule {

    @Binds
    abstract RoutesRepository providesRoutesRepository(RoutesRepositoryImpl repo);

    @Binds
    abstract LocationsRepository providesLocationsRepository(LocationsRepositoryImpl repo);
}
