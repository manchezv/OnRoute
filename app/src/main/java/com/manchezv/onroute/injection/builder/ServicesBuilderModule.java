package com.manchezv.onroute.injection.builder;

import com.manchezv.onroute.injection.subcomponents.service.LocatorServiceModule;
import com.manchezv.onroute.services.LocatorService;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ServicesBuilderModule {

    @ContributesAndroidInjector(modules = LocatorServiceModule.class)
    abstract LocatorService injectsLocatorService();
}
