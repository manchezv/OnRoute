package com.manchezv.onroute.injection.builder;

import com.manchezv.onroute.injection.subcomponents.main.MainActivityModule;
import com.manchezv.onroute.views.main.MainActivity;
import com.manchezv.onroute.views.map.MapActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivitiesBuilderModule {

    @ContributesAndroidInjector(modules = MainActivityModule.class)
    abstract MainActivity injectsMainActivity();

    @ContributesAndroidInjector
    abstract MapActivity injectsMapActivity();
}
