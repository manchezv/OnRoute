package com.manchezv.onroute.injection;

import com.manchezv.onroute.OnRouteApplication;
import com.manchezv.onroute.injection.builder.ActivitiesBuilderModule;
import com.manchezv.onroute.injection.builder.ServicesBuilderModule;
import com.manchezv.onroute.injection.repository.RepositoryModule;
import com.manchezv.onroute.injection.viewmodels.ViewModelsModule;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.support.AndroidSupportInjectionModule;

@Component(
        modules = {
                ApplicationModule.class,
                AndroidSupportInjectionModule.class,
                ActivitiesBuilderModule.class,
                ServicesBuilderModule.class,
                RepositoryModule.class,
                ViewModelsModule.class
        }
)
@Singleton
public interface ApplicationComponent {

    void inject(OnRouteApplication app);


}
