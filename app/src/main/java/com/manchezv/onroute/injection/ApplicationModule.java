package com.manchezv.onroute.injection;

import android.app.Application;
import android.arch.persistence.room.Room;

import com.manchezv.onroute.OnRouteApplication;
import com.manchezv.onroute.model.db.OnRouteDatabase;
import com.manchezv.onroute.model.db.dao.LocationsDAO;
import com.manchezv.onroute.model.db.dao.RoutesDAO;
import com.manchezv.onroute.model.session.RouteSession;
import com.manchezv.onroute.model.session.RouteSessionImpl;
import com.manchezv.onroute.threading.MainThread;
import com.manchezv.onroute.threading.MainThreadImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

    private Application application;

    public ApplicationModule(OnRouteApplication app) {
        application = app;
    }

    @Provides
    @Singleton
    public Application providesApplication() {
        return application;
    }


    @Provides
    @Singleton
    OnRouteDatabase providesOnRouteDatabase(Application app) {
        return Room.databaseBuilder(app, OnRouteDatabase.class, "onroute_db")
                .build();
    }

    @Provides
    @Singleton
    RoutesDAO providesRoutesDAO(OnRouteDatabase db) {
        return db.getRoutesDAO();
    }

    @Provides
    @Singleton
    LocationsDAO providesLocationsDAO(OnRouteDatabase db) {
        return db.getLocationsDAO();
    }

    @Provides
    @Singleton
    RouteSession providesRouteSession(RouteSessionImpl session) {
        return session;
    }

    @Provides
    @Singleton
    MainThread providesMainThread(MainThreadImpl mainThread) {
        return mainThread;
    }

}
