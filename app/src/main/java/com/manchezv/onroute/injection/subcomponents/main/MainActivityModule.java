package com.manchezv.onroute.injection.subcomponents.main;

import android.support.v7.app.AppCompatActivity;

import com.manchezv.onroute.views.main.MainActivity;
import com.manchezv.onroute.views.components.navigator.Navigator;
import com.manchezv.onroute.views.components.navigator.NavigatorImpl;
import com.manchezv.onroute.views.components.sectionnavigator.SectionNavigator;
import com.manchezv.onroute.views.components.sectionnavigator.SectionNavigatorImpl;
import com.manchezv.onroute.views.main.routelist.RouteListFragment;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class MainActivityModule {

    @Binds
    abstract AppCompatActivity providesAppCompatActivity(MainActivity activity);

    @Binds
    abstract SectionNavigator providesSectionNavigator(SectionNavigatorImpl sectionNavigator);

    @Binds
    abstract Navigator providesNavigator(NavigatorImpl navigator);

    @ContributesAndroidInjector
    abstract RouteListFragment injectsRouteListFragment();

}
