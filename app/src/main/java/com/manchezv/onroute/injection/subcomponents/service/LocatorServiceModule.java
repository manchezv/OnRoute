package com.manchezv.onroute.injection.subcomponents.service;

import android.content.Context;

import com.manchezv.onroute.services.LocatorService;
import com.manchezv.onroute.views.components.notifications.NotificationsBuilder;
import com.manchezv.onroute.views.components.notifications.NotificationsBuilderImpl;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class LocatorServiceModule {

    @Binds
    abstract Context providesContext(LocatorService service);

    @Binds
    abstract NotificationsBuilder providesNotificationsBuilder(NotificationsBuilderImpl not);

}
