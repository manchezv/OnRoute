package com.manchezv.onroute.model.repositories;

import android.arch.lifecycle.LiveData;
import android.support.annotation.MainThread;
import android.support.annotation.WorkerThread;

import com.manchezv.onroute.model.RoutesRepository;
import com.manchezv.onroute.model.db.dao.LocationsDAO;
import com.manchezv.onroute.model.db.dao.RoutesDAO;
import com.manchezv.onroute.model.db.entities.LocationEntity;
import com.manchezv.onroute.model.db.entities.RouteEntity;
import com.manchezv.onroute.threading.AppThreadsManager;

import java.util.List;

import javax.inject.Inject;

public class RoutesRepositoryImpl implements RoutesRepository {

    private final RoutesDAO routesDao;
    private final LocationsDAO locationsDao;
    private final AppThreadsManager threadsManager;

    @Inject
    public RoutesRepositoryImpl(RoutesDAO routesDao, LocationsDAO locationsDao, AppThreadsManager threadsManager) {
        this.routesDao = routesDao;
        this.locationsDao = locationsDao;
        this.threadsManager = threadsManager;
    }

    @Override
    @WorkerThread
    public void insert(RouteEntity route) {
        routesDao.insert(route);
    }

    @Override
    @MainThread
    public LiveData<List<RouteEntity>> getRoutes() {
        return routesDao.getRoutes();
    }

    @Override
    public void deleteAll() {
        routesDao.deleteAll();
    }

    @Override
    public void insertRoute(RouteEntity routeEntity, List<LocationEntity> locations) {
        //TODO callback de guardado
        threadsManager.getDiskIO().execute(() -> {
            long routeId = routesDao.insert(routeEntity);
            if (locations != null) {
                for (LocationEntity location : locations) {
                    location.setRouteId(routeId);
                }
                locationsDao.insertAll(locations);
            }
        });
    }

    @Override
    public LiveData<List<RouteListItem>> getRouteListInfo() {
        return routesDao.getRoutesInfo();
    }

}
