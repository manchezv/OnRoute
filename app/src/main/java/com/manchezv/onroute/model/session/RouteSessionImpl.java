package com.manchezv.onroute.model.session;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.location.Location;

import com.manchezv.onroute.model.db.entities.LocationEntity;
import com.manchezv.onroute.model.db.entities.RouteEntity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

public class RouteSessionImpl implements RouteSession {

    private MutableLiveData<Boolean> generatingRoute = new MutableLiveData<>();
    private MutableLiveData<List<LocationEntity>> locationList = new MutableLiveData<>();
    private Date dateStart, dateEnd;

    @Inject
    public RouteSessionImpl() {
    }

    @Override
    public void startRoute() {
        generatingRoute.setValue(true);
        locationList.setValue(new ArrayList<>());
        dateStart = Calendar.getInstance().getTime();
    }

    @Override
    public void stopRoute() {
        generatingRoute.setValue(false);
        dateEnd = Calendar.getInstance().getTime();
    }

    @Override
    public void finishRoute() {
        generatingRoute.setValue(false);
        locationList.setValue(new ArrayList<>());
        dateStart = null;
        dateEnd = null;
    }

    @Override
    public void addLocation(Location location) {
        List<LocationEntity> newLocationList = locationList.getValue();
        if (newLocationList == null) {
            newLocationList = new ArrayList<>();
        }

        newLocationList.add(new LocationEntity(location.getAltitude(), location.getLatitude(), location.getLongitude(), Calendar.getInstance().getTime()));
        locationList.setValue(newLocationList);
    }

    @Override
    public LiveData<Boolean> getServiceState() {
        return generatingRoute;
    }

    @Override
    public LiveData<List<LocationEntity>> getRouteLocations() {
        return locationList;
    }

    @Override
    public final Date getRouteDateStart() {
        return dateStart;
    }

    @Override
    public final Date getRouteDateEnd() {
        return dateEnd;
    }
}
