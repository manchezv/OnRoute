package com.manchezv.onroute.model.db.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.util.TableInfo;

import java.util.Date;

@Entity(tableName = "location_records",
        foreignKeys =
        @ForeignKey(
                entity = RouteEntity.class,
                parentColumns = "id",
                childColumns = "routeId",
                onDelete = ForeignKey.CASCADE,
                onUpdate = ForeignKey.CASCADE
        ))
public class LocationEntity {

    @PrimaryKey(autoGenerate = true)
    private long id;

    private double altitude;

    private double latitude;

    private double longitude;

    @ColumnInfo(name = "generated_timestamp")
    private Date generatedTime;

    private long routeId;

    public LocationEntity(double altitude, double latitude, double longitude, Date generatedTime) {
        this.altitude = altitude;
        this.longitude = longitude;
        this.latitude = latitude;
        this.generatedTime = generatedTime;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getAltitude() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public Date getGeneratedTime() {
        return generatedTime;
    }

    public void setGeneratedTime(Date generatedTime) {
        this.generatedTime = generatedTime;
    }

    public long getRouteId() {
        return routeId;
    }

    public void setRouteId(long routeId) {
        this.routeId = routeId;
    }
}
