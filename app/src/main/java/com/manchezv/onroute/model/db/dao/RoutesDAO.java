package com.manchezv.onroute.model.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.manchezv.onroute.model.db.entities.RouteEntity;
import com.manchezv.onroute.model.repositories.RouteListItem;

import java.util.List;

@Dao
public interface RoutesDAO {

    @Insert
    long insert(RouteEntity route);

    @Query("SELECT * from routes ORDER BY generated_timestamp DESC")
    LiveData<List<RouteEntity>> getRoutes();

    @Query("SELECT r.id as routeId, r.name as routeName, r.startTime as startDate, r.endTime as endDate, COUNT(l.id) as positions from routes r left join location_records l on r.id = l.routeId group by r.id ORDER BY r.generated_timestamp DESC")
    LiveData<List<RouteListItem>> getRoutesInfo();

    @Query("DELETE FROM routes")
    void deleteAll();
}
