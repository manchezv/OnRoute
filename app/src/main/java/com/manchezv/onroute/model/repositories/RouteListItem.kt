package com.manchezv.onroute.model.repositories

import java.util.*

data class RouteListItem(val routeId: Long?,
                         val routeName: String?,
                         val startDate: Date?,
                         val endDate: Date?,
                         val positions: Long?)