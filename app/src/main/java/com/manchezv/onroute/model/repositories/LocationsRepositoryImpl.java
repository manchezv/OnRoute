package com.manchezv.onroute.model.repositories;

import android.arch.lifecycle.LiveData;
import android.support.annotation.MainThread;
import android.support.annotation.WorkerThread;

import com.manchezv.onroute.model.LocationsRepository;
import com.manchezv.onroute.model.RoutesRepository;
import com.manchezv.onroute.model.db.dao.LocationsDAO;
import com.manchezv.onroute.model.db.dao.RoutesDAO;
import com.manchezv.onroute.model.db.entities.LocationEntity;
import com.manchezv.onroute.model.db.entities.RouteEntity;
import com.manchezv.onroute.threading.AppThreadsManager;

import java.util.List;

import javax.inject.Inject;

public class LocationsRepositoryImpl implements LocationsRepository {

    private final LocationsDAO locationsDao;

    @Inject
    public LocationsRepositoryImpl(LocationsDAO locationsDao) {
        this.locationsDao = locationsDao;
    }

    @Override
    public LiveData<List<LocationEntity>> getRoutePositionsFor(long routeId) {
        return locationsDao.getLocationsFor(routeId);
    }
}
