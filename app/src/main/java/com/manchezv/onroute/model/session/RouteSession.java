package com.manchezv.onroute.model.session;

import android.arch.lifecycle.LiveData;
import android.location.Location;

import com.manchezv.onroute.model.db.entities.LocationEntity;

import java.util.Date;
import java.util.List;

public interface RouteSession {
    void startRoute();

    void stopRoute();

    void finishRoute();

    void addLocation(Location location);

    LiveData<Boolean> getServiceState();

    LiveData<List<LocationEntity>> getRouteLocations();

    Date getRouteDateStart();

    Date getRouteDateEnd();
}
