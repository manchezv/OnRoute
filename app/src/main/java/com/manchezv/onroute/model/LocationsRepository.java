package com.manchezv.onroute.model;

import android.arch.lifecycle.LiveData;
import android.support.annotation.MainThread;
import android.support.annotation.WorkerThread;

import com.manchezv.onroute.model.db.entities.LocationEntity;
import com.manchezv.onroute.model.db.entities.RouteEntity;
import com.manchezv.onroute.model.repositories.RouteListItem;

import java.util.List;

public interface LocationsRepository {

    @MainThread
    LiveData<List<LocationEntity>> getRoutePositionsFor(long routeId);

}
