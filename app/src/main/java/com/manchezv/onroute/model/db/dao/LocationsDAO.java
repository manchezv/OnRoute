package com.manchezv.onroute.model.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.manchezv.onroute.model.db.entities.LocationEntity;
import com.manchezv.onroute.model.db.entities.RouteEntity;

import java.util.List;

@Dao
public interface LocationsDAO {

    @Insert
    void insertAll(List<LocationEntity> locations);

    @Query("SELECT * from location_records WHERE routeId=:routeId ORDER BY generated_timestamp ASC")
    LiveData<List<LocationEntity>> getLocationsFor(final long routeId);

    @Query("SELECT * from location_records WHERE routeId=:routeId ORDER BY generated_timestamp DESC LIMIT 1")
    LiveData<LocationEntity> getLastLocationFor(final long routeId);

    @Query("DELETE FROM location_records")
    void deleteAll();

}
