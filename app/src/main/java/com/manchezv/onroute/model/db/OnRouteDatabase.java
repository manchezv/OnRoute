package com.manchezv.onroute.model.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import com.manchezv.onroute.model.db.converter.DataTypesConverter;
import com.manchezv.onroute.model.db.dao.LocationsDAO;
import com.manchezv.onroute.model.db.dao.RoutesDAO;
import com.manchezv.onroute.model.db.entities.LocationEntity;
import com.manchezv.onroute.model.db.entities.RouteEntity;

@Database(entities = {RouteEntity.class, LocationEntity.class}, version = 1)
@TypeConverters(value = {DataTypesConverter.class})
public abstract class OnRouteDatabase extends RoomDatabase {
    public abstract RoutesDAO getRoutesDAO();
    public abstract LocationsDAO getLocationsDAO();
}
