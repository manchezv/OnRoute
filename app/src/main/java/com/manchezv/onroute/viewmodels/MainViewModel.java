package com.manchezv.onroute.viewmodels;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.manchezv.onroute.model.RoutesRepository;
import com.manchezv.onroute.model.db.entities.LocationEntity;
import com.manchezv.onroute.model.db.entities.RouteEntity;
import com.manchezv.onroute.model.repositories.RouteListItem;
import com.manchezv.onroute.model.session.RouteSession;
import com.manchezv.onroute.threading.AppThreadsManager;
import com.manchezv.onroute.utils.RouteListItemMVMapper;
import com.manchezv.onroute.viewmodels.model.RouteListItemModelView;

import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

public class MainViewModel extends ViewModel {

    private final RouteSession session;

    private final LiveData<LocationEntity> currentLocation;
    private final RoutesRepository routesRepository;
    private final AppThreadsManager threadsManager;
    private final RouteListItemMVMapper mapper;

    private LiveData<List<RouteListItemModelView>> routeListInfo;

    @Inject
    public MainViewModel(RouteSession session, RoutesRepository routesRepository, AppThreadsManager threadsManager, RouteListItemMVMapper mapper) {
        this.session = session;
        this.threadsManager = threadsManager;
        this.mapper = mapper;
        this.routesRepository = routesRepository;
        currentLocation = Transformations.map(session.getRouteLocations(), input -> {
            if (input != null && !input.isEmpty()) {
                return input.get(input.size() - 1);
            }
            return null;
        });
    }

    public LiveData<Boolean> isServiceInRoute() {
        return session.getServiceState();
    }

    public LiveData<LocationEntity> getLastLocation() {
        return currentLocation;
    }

    public void onNameInserted(String name) {
        final RouteEntity route = new RouteEntity(name, Calendar.getInstance().getTime(), session.getRouteDateStart(), session.getRouteDateEnd());
        routesRepository.insertRoute(
                route,
                session.getRouteLocations().getValue()
        );
        session.finishRoute();
    }

    public void finishRoute() {
        session.finishRoute();
    }

    public LiveData<List<RouteListItemModelView>> getRouteListInfo() {
        if(routeListInfo == null) {
            routeListInfo = Transformations.map(routesRepository.getRouteListInfo(), new Function<List<RouteListItem>, List<RouteListItemModelView>>() {
                @Override
                public List<RouteListItemModelView> apply(List<RouteListItem> input) {
                    return mapper.reverseMapList(input);
                }
            });
        }
        return routeListInfo;
    }
}
