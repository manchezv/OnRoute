package com.manchezv.onroute.viewmodels.model;

public class RouteListItemModelView {

    private final long routeId;
    private final String routeName;
    private final String startDate;
    private final int hourDuration;
    private final int minuteDuration;
    private final int secondsDuration;
    private final long positions;

    public RouteListItemModelView(long routeId, String routeName, String startDate, int hourDuration, int minuteDuration, int secondsDuration, Long positions) {
        this.routeId = routeId;
        this.routeName = routeName;
        this.startDate = startDate;
        this.hourDuration = hourDuration;
        this.minuteDuration = minuteDuration;
        this.secondsDuration = secondsDuration;
        this.positions = positions;
    }

    public long getRouteId() {
        return routeId;
    }

    public String getRouteName() {
        return routeName;
    }

    public String getStartDate() {
        return startDate;
    }

    public int getHourDuration() {
        return hourDuration;
    }

    public int getMinuteDuration() {
        return minuteDuration;
    }

    public int getSecondsDuration() {
        return secondsDuration;
    }

    public long getPositions() {
        return positions;
    }
}
