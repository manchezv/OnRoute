package com.manchezv.onroute.viewmodels;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.google.android.gms.maps.model.LatLng;
import com.manchezv.onroute.model.LocationsRepository;
import com.manchezv.onroute.model.db.entities.LocationEntity;
import com.manchezv.onroute.model.db.entities.RouteEntity;
import com.manchezv.onroute.model.repositories.RouteListItem;
import com.manchezv.onroute.utils.LocationLatLngMapper;
import com.manchezv.onroute.viewmodels.model.RouteListItemModelView;

import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

public class MapViewModel extends ViewModel {

    private final LocationsRepository locationsRepository;
    private final LocationLatLngMapper mapper;

    private LiveData<List<LocationEntity>> locations;

    private long routeId;

    @Inject
    public MapViewModel(LocationsRepository locationsRepository, LocationLatLngMapper mapper) {

        this.locationsRepository = locationsRepository;
        this.mapper = mapper;
    }

    public LiveData<List<LatLng>> getRoutePositions() {
        if(locations == null) {
            locations = locationsRepository.getRoutePositionsFor(routeId);
        }
        return Transformations.map(locations, mapper::reverseMapList);
    }

    public void setRouteId(long routeId) {
        this.routeId = routeId;
    }
}
